﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
namespace Rousr.ScriptCreator {
    public static class ScriptCreatorUtility {
        private static ScriptCreatorSettings Settings { get; set; }

        public static void FindTemplates() {
            var assets = new List<ScriptTemplate>();
            foreach (var name in AssetDatabase.GetAllAssetPaths()) {
                if (!name.Contains("Assets/")) {
                    continue;
                }
                var asset = AssetDatabase.LoadAssetAtPath<ScriptTemplate>(name);
                if(asset == null) {
                    continue;
                }
                assets.Add(asset);
            }

            var settings = FindSettings();
            settings.Templates = assets;
            EditorUtility.SetDirty(settings);
        }

        public static void CreateScript(ScriptTemplate template, string path) {
            if (File.Exists(path)) {
                throw new System.Exception("File already exists at " + path);
            }
            using (StreamWriter file = new StreamWriter(path)) {
                string body = template.Template;
                template.GenerateDefaultVariables();
                foreach (var v in template.Variables) {
                    string toUpper = "%" + v.key + ":upper%";
                    body = body.Replace(toUpper, v.value.ToUpper());

                    string toLower = "%" + v.key + ":lower%";
                    body = body.Replace(toLower, v.value.ToLower());

                    string toReplace = "%" + v.key + "%";
                    body = body.Replace(toReplace, v.value);
                }
                body = body.Replace("\r", "");
                body = body.Replace("\n", "\r\n");
                file.Write(body);
                file.Close();
                AssetDatabase.Refresh();
                var asset = AssetDatabase.LoadAssetAtPath<MonoScript>(path);
                Selection.activeObject = asset;
            }
        }

        //[MenuItem("Debug/ForceClose")]
        //public static void ForceClose() {
        //    var window = EditorWindow.GetWindow<WizardWindow>();
        //    window.Close();
        //}

        public static void OpenWizard(string templateName) {
            var settings = FindSettings();
            FindTemplates();
            var templates = settings.Templates;
            foreach (var t in templates) {
                if (t.name == templateName) {
                    var asset = t;
                    //var window = ScriptableObject.CreateInstance<WizardWindow>();
                    var window = EditorWindow.GetWindow<WizardWindow>();
                    //window.ShowPopup();
                    window.Show();
                    string path = FindPathFromSelection();
                    window.Set(asset, path);
                    window.position = new Rect((Screen.currentResolution.width - 500) / 2, (Screen.currentResolution.height - 200) / 2, 500, 200);
                    return;
                }
            }

            throw new System.Exception("Could not find template " + templateName);
        }

        private static string FindPathFromSelection() {
            var path = "";
            var obj = Selection.activeObject;
            if (obj == null) path = "Assets";
            else path = AssetDatabase.GetAssetPath(obj.GetInstanceID());

            if (File.Exists(path)) {
                return Path.GetDirectoryName(path);
            }else if (Directory.Exists(path)) {
                return path;
            }

            throw new System.Exception("Unable to determine path");
        }

        //[MenuItem("Tools/Script Creator Utility/Debug/Generate Menu Items")]
        public static void GenerateMenuItems() {
            var settings = FindSettings();
            string homeDir = Path.GetDirectoryName(AssetDatabase.GetAssetPath(settings));
            string path = homeDir + "/Templates";
            string functions = "";
            foreach (var asset in settings.Templates) {
                string fn = "   [MenuItem(\"Assets/Create/";

                string menuName = asset.MenuName;
                if (menuName.Substring(0,2) == "..") {
                    fn += menuName.Substring(3);
                } else {
                    fn += settings.MenuName + "/" + menuName;
                }

                fn += "\", priority = " + settings.Priority + ")]\n" +
                    "   public static void Create" + asset.GetHashCode().ToString().Replace("-", "_") + "(){\n" +
                    "       ScriptCreatorUtility.OpenWizard(\"" + asset.name + "\");\n" +
                    "   }\n\n";
                functions += fn;
            }

            var scriptPath = homeDir + "\\Utility\\MenuItems.cs";
            if (!File.Exists(scriptPath)) {
                throw new System.Exception("Expected script at " + scriptPath);
            }

            File.WriteAllText(scriptPath, "");
            AssetDatabase.Refresh();

            string scriptContents = "" +
                "using UnityEngine;\n" +
                "using UnityEditor;\n" +
                "namespace Rousr.ScriptCreator{\n" +
                "   public static class MenuItems{\n" +
                "   " + functions + "\n" +
                "   }\n}\n";

            File.WriteAllText(scriptPath, scriptContents);
            AssetDatabase.Refresh();
        }

        //[MenuItem("Tools/Script Creator Utility/Debug/Find Home")]
        public static ScriptCreatorSettings FindSettings() {
            if(Settings != null) {
                return Settings;
            }
            ScriptCreatorSettings asset = null;
            var type = typeof(ScriptCreatorSettings);

            var guids = AssetDatabase.FindAssets(type.Name);
            foreach (var guid in guids) {
                var path = AssetDatabase.GUIDToAssetPath(guid);
                asset = AssetDatabase.LoadAssetAtPath<ScriptCreatorSettings>(path);
                if (asset == null) {
                    continue;
                } else {
                    break;
                }
            }

            Settings = asset ?? throw new System.Exception("Could not find Script Creator Settings");
            return Settings;
        }

        [MenuItem("Tools/Rousr/Script Creator/Refresh Project")]
        public static void RefreshProject() {
            FindTemplates();
            GenerateMenuItems();
        }
    }
}