using UnityEngine;
using UnityEditor;
namespace Rousr.ScriptCreator{
   public static class MenuItems{
      [MenuItem("Assets/Create/bbjeans/SerializedClass", priority = 80)]
   public static void Create_18174(){
       ScriptCreatorUtility.OpenWizard("SerializedClass");
   }

   [MenuItem("Assets/Create/bbjeans/MonoBehavior", priority = 80)]
   public static void Create11972(){
       ScriptCreatorUtility.OpenWizard("MonoBehavior");
   }

   [MenuItem("Assets/Create/bbjeans/ScriptableObject", priority = 80)]
   public static void Create12206(){
       ScriptCreatorUtility.OpenWizard("ScriptableObject");
   }

   [MenuItem("Assets/Create/bbjeans/StaticClassExtension", priority = 80)]
   public static void Create12302(){
       ScriptCreatorUtility.OpenWizard("StaticExtensionClass");
   }

   [MenuItem("Assets/Create/bbjeans/CustomPropertyDrawer", priority = 80)]
   public static void Create12540(){
       ScriptCreatorUtility.OpenWizard("CustomPropertyDrawer");
   }

   [MenuItem("Assets/Create/FamiliarFactory/ActorBehavior", priority = 80)]
   public static void Create12856(){
       ScriptCreatorUtility.OpenWizard("ActorBehavior");
   }

   [MenuItem("Assets/Create/bbjeans/CustomInspector", priority = 80)]
   public static void Create12876(){
       ScriptCreatorUtility.OpenWizard("CustomInspector");
   }


   }
}
