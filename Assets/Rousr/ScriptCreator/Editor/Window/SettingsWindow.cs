﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Rousr.ScriptCreator {
    public class SettingsWindow : EditorWindow {

        [MenuItem("Tools/Rousr/Script Creator/Settings")]
        public static void OpenWindow() {
            var window = GetWindow<SettingsWindow>();
            window.Show();
        }

        private Editor mEditor;
        private ScriptCreatorSettings mSettings;
        private ScriptCreatorSettings Settings {
            get {
                if (mSettings == null) {
                    mSettings = ScriptCreatorUtility.FindSettings();
                }
                return mSettings;
            }
        }

        private void Awake() {
            titleContent = new GUIContent("Script Creator Settings");
        }

        private void OnGUI() {
            Editor.CreateCachedEditor(Settings, typeof(ScriptCreatorSettingsInspector), ref mEditor);
            if (mEditor != null) {
                mEditor.OnInspectorGUI();
            }
        }
    }
}