﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using UnityEngine;
using UnityEditor;

namespace Rousr.ScriptCreator {
    public class WizardWindow : EditorWindow {
        public ScriptTemplate CurrentTemplate;
        public List<ScriptTemplate.Variable> CurrentVariables = new List<ScriptTemplate.Variable>();
        public string Path;

        public void Set(ScriptTemplate template, string path) {
            CurrentTemplate = template;
            Path = path;
        }

        private bool _Focused = false;
        private void OnGUI() {
            bool enterPressed = false;
            bool escapePressed = false;

            if (focusedWindow == this && Event.current.type == EventType.KeyUp) {
                enterPressed  = Event.current.keyCode == KeyCode.Return || Event.current.keyCode == KeyCode.KeypadEnter;
                escapePressed = Event.current.keyCode == KeyCode.Escape;
            }

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Create New " + CurrentTemplate.MenuName);
            EditorGUILayout.LabelField("Filename", CurrentTemplate.FileName);

            for (int i = 0; i < CurrentTemplate.Variables.Count; i++) {
                var v = CurrentTemplate.Variables[i];

                if (v.key == "EXTENSION" || v.key == "MENU_NAME")
                    continue;

                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;

                EditorGUI.BeginChangeCheck();
                if (!_Focused)
                    GUI.SetNextControlName("ScriptWizardFirstFocus");

                v.value = EditorGUILayout.TextField(textInfo.ToTitleCase(v.key), v.value);

                if (!_Focused) {
                    EditorGUI.FocusTextInControl("ScriptWizardFirstFocus");
                    _Focused = true;
                }

                if (EditorGUI.EndChangeCheck())
                    CurrentTemplate.SetVariable(v.key, v.value);
            }

            if (GUILayout.Button("Create") || enterPressed) { 
                ScriptCreatorUtility.CreateScript(CurrentTemplate, Path + "/" + CurrentTemplate.FileName);
                Close();
            }

            if (GUILayout.Button("Cancel") || escapePressed) {
                Close();
            }
        }
    }
}