using UnityEditor;
using UnityEngine;

namespace Rousr.ScriptCreator {
    [CustomEditor(typeof(ScriptCreatorSettings))]
    public class ScriptCreatorSettingsInspector : Editor {

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            EditorGUILayout.LabelField("Registered Templates");
            EditorGUI.indentLevel++;
            var templatesProp = serializedObject.FindProperty("Templates");
            for (int i = 0; i < templatesProp.arraySize; i++) {
                EditorGUILayout.ObjectField(i.ToString(), templatesProp.GetArrayElementAtIndex(i).objectReferenceValue, typeof(ScriptTemplate), true);
            }
            EditorGUI.indentLevel--;

            EditorGUILayout.Space();
            if (GUILayout.Button("Search For Templates")) {
                ScriptCreatorUtility.FindTemplates();
                ScriptCreatorUtility.GenerateMenuItems();
            }

            if (GUILayout.Button("Clear Templates")) {
                var settings = ScriptCreatorUtility.FindSettings();
                settings.Templates.Clear();
                EditorUtility.SetDirty(settings);
            }

            if (GUILayout.Button("Generate Menu Items")) {
                ScriptCreatorUtility.GenerateMenuItems();
            }
        }
    }
}