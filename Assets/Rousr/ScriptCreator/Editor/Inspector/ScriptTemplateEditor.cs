﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace Rousr.ScriptCreator {
    [CustomEditor(typeof(ScriptTemplate))]
    public class ScriptTemplateEditor : Editor {
        private ScriptCreatorSettings mSettings;

        public override void OnInspectorGUI() {
            if (mSettings == null) {
                mSettings = ScriptCreatorUtility.FindSettings();
            }

            if (!mSettings.Templates.Contains(target as ScriptTemplate)) {
                mSettings.Templates.Add(target as ScriptTemplate);
                ScriptCreatorUtility.GenerateMenuItems();
            }

            if (string.IsNullOrEmpty(serializedObject.FindProperty("MenuName").stringValue)) {
                serializedObject.FindProperty("MenuName").stringValue = target.name;
                serializedObject.ApplyModifiedProperties();
                EditorUtility.SetDirty(target);
            }

            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("MenuName"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("NamingExpression"));
            if (EditorGUI.EndChangeCheck()) {
                serializedObject.ApplyModifiedProperties();
                EditorUtility.SetDirty(target);
            }
            //EditorGUILayout.LabelField("Utility");
            //EditorGUI.indentLevel++;
            //if (GUILayout.Button("Generate Menu Item")) {
            //    ScriptCreatorUtility.GenerateMenuItems();
            //}
            //EditorGUI.indentLevel--;

            EditorGUILayout.LabelField("Variables");
            EditorGUI.indentLevel++;
            var variablesProp = serializedObject.FindProperty("mVariables");
            for (var i = 0; i < variablesProp.arraySize; i++) {
                var varProp = variablesProp.GetArrayElementAtIndex(i);
                var keyProp = varProp.FindPropertyRelative("key");
                var valueProp = varProp.FindPropertyRelative("value");

                EditorGUILayout.BeginHorizontal("box");
                EditorGUILayout.LabelField(i.ToString(), GUILayout.Width(25));
                EditorGUI.BeginChangeCheck();
                keyProp.stringValue = EditorGUILayout.TextField(keyProp.stringValue);
                valueProp.stringValue = EditorGUILayout.TextField(valueProp.stringValue);
                if (EditorGUI.EndChangeCheck()) {
                    serializedObject.ApplyModifiedProperties();
                    EditorUtility.SetDirty(target);
                }
                if (GUILayout.Button("Delete")) {
                    variablesProp.DeleteArrayElementAtIndex(i);
                    i--;
                    serializedObject.ApplyModifiedProperties();
                    EditorUtility.SetDirty(target);
                }
                EditorGUILayout.EndHorizontal();
            }
            if (GUILayout.Button("New Variable")) {
                variablesProp.arraySize++;
            }
            EditorGUI.indentLevel--;

            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Template"));
            if (EditorGUI.EndChangeCheck()) {
                serializedObject.ApplyModifiedProperties();
                EditorUtility.SetDirty(target);
            }

            //base.OnInspectorGUI();
        }
    }
}
