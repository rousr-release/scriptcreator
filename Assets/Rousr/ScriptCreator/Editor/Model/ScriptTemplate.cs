﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Rousr.ScriptCreator {
    [CreateAssetMenu(menuName = "Script Creator Template", order = 100)]
    public class ScriptTemplate : ScriptableObject {

        [System.Serializable]
        public struct Variable {
            public string key;
            public string value;
        }

        public string MenuName;
        public string Extension = "cs";
        public string NamingExpression = "%CLASS%.%EXTENSION%";

        [SerializeField]
        private List<Variable> mVariables = new List<Variable>() { new Variable() { key = "CLASS", value = "NewMonoBehaviour" } };
        public List<Variable> DefaultVariables { get; set; } = new List<Variable>();
        public List<Variable> Variables => mVariables.Concat(DefaultVariables).ToList();

        [TextArea(64, 999)]
        public string Template;

        public string FileName {
            get {
                GenerateDefaultVariables();
                string fileName = NamingExpression;
                foreach (var v in Variables) {
                    fileName = fileName.Replace("%" + v.key + "%", v.value);
                }
                return fileName;
            }
        }

        public void GenerateDefaultVariables() {
            DefaultVariables.Clear();
            DefaultVariables.Add(new Variable() { key = "MENU_NAME", value = MenuName });
            DefaultVariables.Add(new Variable() { key = "EXTENSION", value = Extension });
        }

        public bool TryGetVariable(string name, out Variable found) {
            found = default;

            foreach (var v in mVariables) {
                if (v.key == name) {
                    found = v;
                    return true;
                }
            }
            return false;
        }

        public void SetVariable(string name, string value) {
            for (var i = 0; i < mVariables.Count; i++) {
                var v = mVariables[i];
                if (v.key == name) {
                    v.value = value;
                    mVariables[i] = v;
                    return;
                }
            }

            mVariables.Add(new Variable() { key = value, value = value });
        }
    }
}