﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rousr.ScriptCreator {
    //[CreateAssetMenu(menuName = "ScriptCreatorSettings")]
    public class ScriptCreatorSettings : ScriptableObject {
        public int Priority = -1;
        public string MenuName = "Scripts";
        [HideInInspector]
        public List<ScriptTemplate> Templates = new List<ScriptTemplate>();
    }
}