﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace Rousr.Toolkit {
    [CustomPropertyDrawer(typeof(AssetDirectoryAttribute))]
    public class AssetDirectoryPropertyDrawer : PropertyDrawer {

        public bool TryGetDragPath(Rect rect, out string dragPath) {
            dragPath = "";

            if (rect.Contains(Event.current.mousePosition)) {
                if (DragAndDrop.objectReferences.Length > 0) {
                    var dragged = DragAndDrop.objectReferences[0];
                    if (dragged.GetType() == typeof(DefaultAsset)) {
                        dragPath = AssetDatabase.GetAssetPath(dragged);
                        if (!Directory.Exists(dragPath)) {
                            dragPath = Path.GetDirectoryName(dragPath);
                        }
                        return true;
                    }
                }
            }

            return false;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            bool didChange = false;
            int lastIndent = EditorGUI.indentLevel;
            EditorGUI.BeginProperty(position, label, property);
            var contentRect = EditorGUI.PrefixLabel(position, label, EditorStyles.label);
            EditorGUI.indentLevel = 0;
            float buttonWidth = 25;
            float buttonHeight = 16;
            float padding = 5;
            float buttonRectPaddingY = -0.5f;
            float textWidth = contentRect.width - buttonWidth;

            var textRect = new Rect(contentRect) { width = contentRect.width - buttonWidth - padding };
            var buttonRect = new Rect(contentRect) { width = buttonWidth, height = buttonHeight };
            buttonRect.x += textRect.width + padding;
            buttonRect.y += buttonRectPaddingY;


            string readDir = property.stringValue;

            var labelStyle = new GUIStyle(EditorStyles.textField);


            if (TryGetDragPath(textRect, out string dragPath)) {
                readDir = dragPath;
                if (Event.current.type == EventType.DragUpdated) {
                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                    Event.current.Use();
                } else if (Event.current.type == EventType.DragPerform) {
                    property.stringValue = dragPath;
                    didChange = true;
                    Event.current.Use();
                }
            }

            // draw disabled-looking text
            labelStyle.normal.textColor = new Color(1, 1, 1, 0.3f);
            EditorGUI.LabelField(textRect, readDir, labelStyle);

            if (readDir != dragPath) {
                EditorGUI.DrawRect(textRect, new Color(0, 0, 0, 0.05f));
            }

            if (GUI.Button(buttonRect, "...")) {
                string initialDir = Directory.Exists(property.stringValue) ? property.stringValue : Application.dataPath;
                string userDir = EditorUtility.OpenFolderPanel("Choose a folder", initialDir, "");
                if (userDir != property.stringValue && userDir != "") {
                    if (!userDir.Contains(Application.dataPath)) {
                        throw new System.Exception("Cannot select a path outside of the main Assets folder");
                    }
                    property.stringValue = userDir.Replace(Application.dataPath.Replace("/Assets", ""), "");

                    didChange = true;
                }
            }

            // keep normalized
            if (didChange) {
                GUI.changed = true;
                property.stringValue = property.stringValue.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                char firstLetter = property.stringValue.ToCharArray()[0];
                if (firstLetter == Path.AltDirectorySeparatorChar) {
                    property.stringValue = property.stringValue.Substring(1);
                }
            }

            EditorGUI.EndProperty();
            EditorGUI.indentLevel = lastIndent;
        }
    }
}
