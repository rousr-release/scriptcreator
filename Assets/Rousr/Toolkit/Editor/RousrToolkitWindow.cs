using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Type = System.Type;
using System.Linq;

namespace Rousr.Toolkit {
    public class RousrToolkitWindow : EditorWindow {
        private static bool _InspectorIsRousrWindow;
        public static bool InspectorIsRousrWindow => _InspectorIsRousrWindow;

        // not enough content to bother the user with this
        [MenuItem("Window/Rousr Tool Kit")]
        public static RousrToolkitWindow OpenSelf() {
            var window = GetWindow<RousrToolkitWindow>();
            window.Show();
            return window;
        }

        private RousrToolkit mRousrToolkit;
        private Dictionary<Type, Editor> mCachedEditors = new Dictionary<Type, Editor>();

        private RousrTool mCurrentChild;

        private void OnEnable() {
            mCachedEditors.Clear();
            mRousrToolkit = RousrToolkitUtility.Toolkit;
            mCurrentChild = mRousrToolkit.Children.Count > 0 ? mRousrToolkit.Children[0] : null;
            Postprocess.ToolkitPostprocessor.AfterAllAssetsProcess.AddListener(() => {
                mCachedEditors = new Dictionary<Type, Editor>();
            });
        }

        public TEditor SetTool<TSettings, TEditor>() where TEditor : Editor {
            var asset = mRousrToolkit.Children.Where(c => c.GetType() == typeof(TSettings)).First();
            if (!asset) {
                throw new System.Exception("No registered tool " + typeof(TSettings));
            }

            mCurrentChild = asset;
            if (!mCachedEditors.TryGetValue(typeof(TSettings), out Editor editor)) {
                Editor.CreateCachedEditor(asset, null, ref editor);
                mCachedEditors.Add(typeof(TSettings), editor as RousrToolEditor);
            }

            return editor as TEditor;
        }

        public Editor SetTool<TSettings>() {
            return SetTool<TSettings, Editor>();
        }

        private void OnGUI() {
            _InspectorIsRousrWindow = true;
            DrawTabs();
            if (mCurrentChild == null) {
                // in case something went wrong like the assets were delete 
                // attempt to bind them again as they've likely already regenerated
                mRousrToolkit = RousrToolkitUtility.Toolkit;
                if (mRousrToolkit.Children.Count > 0) {
                    mCurrentChild = mRousrToolkit.Children[0];
                    DrawChildEditor(mCurrentChild);
                } else {
                    EditorGUILayout.HelpBox("No tools found!", MessageType.Warning);
                }
            } else {
                DrawChildEditor(mCurrentChild);
            }
            _InspectorIsRousrWindow = false;
        }

        private void DrawTabs() {
            EditorGUILayout.BeginHorizontal();
            foreach (var child in mRousrToolkit.Children) {
                if (GUILayout.Button(child.GetType().ToString())) {
                    mCurrentChild = child;
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        private void DrawChildEditor(RousrTool child) {
            mCachedEditors.TryGetValue(child.GetType(), out Editor childEditor);
            Editor.CreateCachedEditor(child, null, ref childEditor);
            mCachedEditors[child.GetType()] = childEditor;
            childEditor.OnInspectorGUI();
        }
    }
}