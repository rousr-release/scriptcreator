﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Rousr.Toolkit {
    [CustomEditor(typeof(RousrToolkit))]
    public class RousrToolkitEditor : Editor {
        public override void OnInspectorGUI() {
            EditorGUILayout.HelpBox("I am the RousrToolkit asset! Don't delete me", MessageType.Warning); 
        }
    }
}
