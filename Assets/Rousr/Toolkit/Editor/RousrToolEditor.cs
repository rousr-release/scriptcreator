﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Rousr.Toolkit {
    [CustomEditor(typeof(RousrTool), true)]
    public class RousrToolEditor : Editor {
        public override void OnInspectorGUI() {
            EditorGUI.BeginChangeCheck();
            // draw things normally, omitting the script
            SerializedProperty property = serializedObject.GetIterator();
            bool expanded = true;
            while (property.NextVisible(expanded)) {
                if (property.propertyPath != "m_Script") {
                    EditorGUILayout.PropertyField(property, true);
                }
                expanded = false;
            }
            if (EditorGUI.EndChangeCheck()) {
                EditorUtility.SetDirty(target);
                serializedObject.ApplyModifiedProperties();
            }
        }
    }
}
