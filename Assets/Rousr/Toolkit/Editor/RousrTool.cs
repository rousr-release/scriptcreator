using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rousr.Toolkit {
    public abstract class RousrTool : ScriptableObject {
        public abstract string Name { get; }
    }
}