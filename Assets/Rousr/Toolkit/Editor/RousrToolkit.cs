﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

namespace Rousr.Toolkit {
    public class RousrToolkit : RousrTool {
        public override string Name => "RousrToolkit";

        public List<RousrTool> Children = new List<RousrTool>();

        public T FindTool<T>() where T : RousrTool => FindTool(typeof(T)) as T;
        public RousrTool FindTool(Type type) => Children.Find(c => c.GetType() == type);
    }
}
