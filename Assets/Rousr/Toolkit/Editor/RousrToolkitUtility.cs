﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using UnityEditor;
using Type = System.Type;
using System.IO;

namespace Rousr.Toolkit {
    public static class RousrToolkitUtility {
        private static RousrToolkit _Toolkit;

        public static RousrToolkit Toolkit {
            get {
                if (_Toolkit == null) {
                    _Toolkit = GitKit();
                }
                return _Toolkit;
            }
        }

        public static T FindTool<T>() where T : RousrTool {
            return Toolkit.FindTool<T>();
        }

        private static RousrToolkit GitKit() {
            var expectedPath = AssetDatabase.FindAssets("RousrToolkit t:MonoScript");
            foreach (var p in expectedPath) {
                var assetPath = AssetDatabase.GUIDToAssetPath(p);
                var asset = AssetDatabase.LoadAssetAtPath<MonoScript>(assetPath);
                if (asset == null) {
                    continue;
                }
                if (asset.name == "RousrToolkit") {
                    string toolkitPath = Directory.GetParent(Directory.GetParent(Path.GetDirectoryName(assetPath)).ToString()) + "\\RousrToolkit.asset";
                    var kit = AssetDatabase.LoadAssetAtPath<RousrToolkit>(toolkitPath);
                    if (!kit) {
                        kit = ScriptableObject.CreateInstance<RousrToolkit>();
                        AssetDatabase.CreateAsset(kit, toolkitPath);
                        EditorUtility.DisplayDialog("Welcome to RousrToolkit", "Rousr Toolkit has been installed! Make sure to keep the RousrToolkit.asset file in the same location relative to the parent Rousr folder. If you delete this asset you will lose any RousrTool settings and you will be shown this dialog again.", "Sounds scary but okay");
                        Selection.activeObject = kit;
                    }
                    return kit;
                }
            }
            return null;
        }

        public static void AddSettingsToToolkit(RousrTool toolSettings) {
            var tk = Toolkit;

            if (tk == null) {
                throw new System.Exception("Could not locate RousrToolkit");
            }

            if (toolSettings == null) {
                throw new System.Exception("Null tool settings");
            }

            tk.Children.Add(toolSettings);
            toolSettings.hideFlags = HideFlags.HideInInspector;
            AssetDatabase.AddObjectToAsset(toolSettings, tk);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            EditorUtility.DisplayDialog("New RousrTool installed", toolSettings.Name + " has been installed! All saved settings for this tool will live in the RousrToolkit asset", "Got it!");
        }
    }
}
