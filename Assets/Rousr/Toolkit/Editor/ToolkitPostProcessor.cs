﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System.Linq;
using System;
using Object = UnityEngine.Object;
using UnityEngine.Events;

namespace Rousr.Toolkit.Postprocess {
    public class ToolkitInitAttribute : Attribute { }

    [InitializeOnLoad]
    public partial class ToolkitPostprocessor : AssetPostprocessor {
        public struct ImportListener {
            public Action<Object> action;
            public string targetGuid;
            public string targetPath;
        }

        private static List<Type> _RequiredAssets = new List<Type>();
        private static List<ImportListener> _OnImportListeners = new List<ImportListener>();
        private static Action<string> OnMoveAsset;
        private static bool _IsInitialized = false;

        public static UnityEvent AfterAllAssetsProcess = new UnityEvent();

        public static void RequireAsset<T>() {
            _RequiredAssets.Add(typeof(T));
        }

        static ToolkitPostprocessor() {
            OnInit();
        }

        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths) {
            foreach (var item in movedAssets) {
                OnMoveAsset?.Invoke(item);
            }

            OnInit();
            var listeners = _OnImportListeners;
            _OnImportListeners = new List<ImportListener>();

            foreach (var path in importedAssets) {
                for (int i = 0; i < listeners.Count; i++) {
                    var listener = listeners[i];
                    if (
                        (listener.targetGuid == null && listener.targetPath == null) || // May be called any time
                        (listener.targetPath == path || listener.targetGuid == AssetDatabase.AssetPathToGUID(path))) // must match 
                    {
                        listeners.RemoveAt(i);
                        i--;
                        if (listener.action != null) {
                            listener.action.Invoke(AssetDatabase.LoadAssetAtPath<Object>(path));
                        }
                    }
                }
            }
            AfterAllAssetsProcess.Invoke();
        }

        public static void WaitForImportWithPath(string path, Action<Object> action) {
            _OnImportListeners.Add(new ImportListener() {
                targetPath = path.Replace("\\", "/"), // probably not the correct way to do this
                action = action
            });
        }

        public static void WaitForImportWithGUID(string guid, Action<Object> action) {
            _OnImportListeners.Add(new ImportListener() {
                targetGuid = guid,
                action = action
            });
        }

        public static void WaitForImport(Action<Object> action) {
            _OnImportListeners.Add(new ImportListener() {
                action = action
            });
        }

        private static void OnInit() {
            if (_IsInitialized) return;
            _RequiredAssets = new List<Type>();
            typeof(ToolkitPostprocessor).GetMethods(BindingFlags.Static | BindingFlags.NonPublic)
                .Where(methodInfo => methodInfo.GetCustomAttribute<ToolkitInitAttribute>() != null)
                .Select(m => m.Invoke(null, null))
                .ToList();

            var toolkit = RousrToolkitUtility.Toolkit;
            foreach (var child in toolkit.Children) {
                int foundIndex = _RequiredAssets.IndexOf(child.GetType());
                if (foundIndex >= 0) {
                    _RequiredAssets.RemoveAt(foundIndex);
                }
            }

            foreach (var type in _RequiredAssets) {
                if (!type.IsSubclassOf(typeof(RousrTool))) {
                    throw new System.Exception("Could not register type of " + type.ToString());
                }
                var inst = ScriptableObject.CreateInstance(type) as RousrTool;
                inst.name = type.Name;
                RousrToolkitUtility.AddSettingsToToolkit(inst);
            }
        }
    }
}
